import {somar, somarAsync, chamarFuncao, stringbinarioParaNumber} from './funcoes';

describe('Testar funcoes', () => {
    describe('somar', () => {
        test('1+2 deve ser igual a 3', () => {
            const resultado = somar(1,2);
            expect(resultado).toBe(3);
        });
        test('1+0 deve ser igual a 1', () => {
            const resultado = somar(1,0);
            expect(resultado).toBe(1);
        });
    });
    describe('somarAsync', () => {
        test('1+1 deve ser igual a 2', async () => {
            const resultado = await somarAsync(1,1);
            expect(resultado).toBe(2);
        });
    });
    describe('chamarFuncao', () => {
        test('deve chamar callback', () => {
            const callback = jest.fn(); //usar um mock para uma função
            chamarFuncao(callback);
            expect(callback).toHaveBeenCalledTimes(1);
        });
    });
    describe('stringbinarioParaNumber', () => {
        test.each([['0',0],['1',1],['11',3]])('%s converte para %i', (bin, int) => {
            const resultado = stringbinarioParaNumber(bin);
            expect(resultado).toBe(int);
        });
        test('aa gera exceção', () => {
            expect(() => {stringbinarioParaNumber("aa")}).toThrow(Error);
        });
    });
});